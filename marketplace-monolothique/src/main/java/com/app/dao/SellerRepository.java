package com.app.dao;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.entities.Seller;


public interface SellerRepository extends MongoRepository<Seller, String> {
  Seller  findByMailAndPassword(String mail,String password);
}
