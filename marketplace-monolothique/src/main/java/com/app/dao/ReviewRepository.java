package com.app.dao;


import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.app.entities.Review;


public interface ReviewRepository extends MongoRepository<Review, String>,QuerydslPredicateExecutor<Review> {

	
	public List<Review> findByProduct(String id);
	public List<Review> findByCustomer(String id);

}
