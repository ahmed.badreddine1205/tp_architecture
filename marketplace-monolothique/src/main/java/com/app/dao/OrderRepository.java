package com.app.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import com.app.entities.Order;


public interface OrderRepository extends MongoRepository<Order, String> ,QuerydslPredicateExecutor<Order> {
	List<Order> findByCustomer(String id);

}
