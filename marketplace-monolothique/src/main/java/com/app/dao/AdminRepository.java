package com.app.dao;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.app.entities.Admin;


public interface AdminRepository extends MongoRepository<Admin, String> {
	
	Admin findOneByRole(String role);
	List<Admin> findByRole(String role);
	Admin findByMailAndPassword(String mail,String password);

}
