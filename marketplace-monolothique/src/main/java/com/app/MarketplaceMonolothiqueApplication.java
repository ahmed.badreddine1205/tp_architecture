package com.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MarketplaceMonolothiqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(MarketplaceMonolothiqueApplication.class, args);
	}

}
